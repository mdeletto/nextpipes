# NextPipes

NextPipes is a series of Nextflow pipelines for various types of bioinformatics analyses. 
The concept behind NextPipes was to create a simple, reproducible, and easily configurable series of pipelines with minimal dependencies. 
Ideally, NextPipes is run in a containerized environment such as Docker or Singularity, 
and can be easily adapted to a variety of execution environments both locally and in the cloud.

It is suggested that the user familiarize themselves with the [Nextflow DSL](https://www.nextflow.io/docs/latest/basic.html). This will provide you with a background on the more advanced configuration options, and how NextPipes actually runs under the hood should you choose to explore the source code further.

## Installation

1) Clone the NextPipes project from the Gitlab repository.

   ```bash
   git clone https://gitlab.partners.org/ksg/hector/nextpipes/nextpipes.git
   cd nextpipes
   ```

5) For convenience, and to ensure NextPipes behaves as expected, add the following to `~/.bashrc` replacing `/path/to` with the actual absolute path of your NextPipes repo.

   ```bash
   export NEXTPIPES=/path/to/nextpipes/
   ```

3) Install the Java SDK [>= 11, <= 18](https://www.nextflow.io/docs/latest/getstarted.html#requirements). 
On [Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-20-04):

    ```bash
    sudo apt update && apt install default-jdk
    ```

4) Run the `install.sh` script to install the nextflow binary. Alternatively, you can follow the installation instructions
for Nextflow as described [here](https://www.nextflow.io/docs/latest/getstarted.html#installation). 
Optionally, move the nextflow file to a directory accessible by your `$PATH`. 
Otherwise you will need to call nextflow via its absolute or relative path.

**At this point, you've completed the most basic form of installation**. 
However, stopping here limits you from leveraging the full power of NextPipes, and you'd need all process dependencies
for your pipeline installed in your local environment. Therefore, it is **highly** suggested you proceed with the next steps.

5) Install one or more containerization technologies (e.g. 
[Docker](https://docs.docker.com/engine/install/),
[Apptainer/Singularity](https://github.com/apptainer/apptainer/blob/main/INSTALL.md), 
[Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html#)). 
Installation for each of these is outside the scope of this guide; you should consult the installation guides for each of these respectively.

Assuming everything went smoothly during installation, you should be able to run any NextPipes pipeline using the `nextflow` command.

### Let's run a test

```bash
# Basic local execution
$ nextflow run ./workflows/test.nf -profile local -c nextflow.config
# Using a containerization profile such as Docker
$ nextflow run ./workflows/test.nf -profile docker -c nextflow.config

# Assuming everything went smoothly, you should see output similar to the following:
N E X T F L O W  ~  version 23.10.0
Launching `workflows/test.nf` [happy_hawking] DSL2 - revision: 4d39e91e29
executor >  local (1)
[ec/526b09] process > printTest [100%] 1 of 1 ✔
Congrats! You just ran a test with the following Nextflow profiles: local
```

If everything went smoothly, you should see "Congrats! You just ran a test with the following Nextflow profiles: ${profiles}". 
This workflow runs a simple echo statement along with the nextflow profiles run with this workflow instance.

## Profiles

A Nextflow profile is a set of configuration attributes that can be selected during pipeline execution by using the `-profile` command line option.

Configuration profiles are defined by using the special scope `profiles`, 
which group the attributes that belong to the same profile using a common prefix. For example:

```text
 profiles {

    standard {
        process.executor = 'local'
    }

    cluster {
        process.executor = 'sge'
        process.queue = 'long'
        process.memory = '10GB'
    }

    cloud {
        process.executor = 'cirrus'
        process.container = 'cbcrg/imagex'
        docker.enabled = true
    }

}
```

This configuration defines three different profiles: standard, cluster, and cloud, that each set different process configuration strategies depending on the target runtime platform. The standard profile is used by default when no profile is specified.

Multiple configuration profiles can be specified by separating the profile names with a comma (e.g. `-profile profile1,profile2`).

To get you started using profiles, we've included some basic profile template examples to get you started. 
These can be found under the `$NEXTPIPES/conf/` directory, and you'll find various groups of profiles addressing:

- Containerization
- Executors
- General Settings
- Reference Files
- Resource Allocation

For a complete list of configuration scopes to build your profiles, please refer to Nextflow's documentation [here](https://www.nextflow.io/docs/latest/config.html#config-scopes).
