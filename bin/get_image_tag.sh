#!/usr/bin/env bash

# shellcheck source=./../../incl.sh
. "$(dirname "$(readlink -f "$0")")"/images.sh

set -o errexit
set -o nounset
set -o pipefail

usage() {
  echo "Usage: get_image_tag FILE KEY"
  echo "From a json of images, get the value"
  echo "DESCRIPTION"
  echo "Default images.json or test images.json can be used here. If no value is found, nothing is returned (exit 1)" | fold | awk '{ print "\t" $0 }'
  echo "INPUTS"
  printf "\t%10s %20s\n" "FILE" "Location of images JSON file"
  printf "\t%10s %20s\n" "KEY" "String of key to get docker image ref"
  echo ""
}

if [ -z "${1-}" ] || [ -z "${2-}" ]; then
  usage
  exit 1
fi

contents="$(cat "$1")"

result="$(get_image_tag "$contents" "$2")"

if [ -z "$result" ] || [ "null" == "$result" ]; then
  exit 1
fi

sed -e 's/^"//' -e 's/"$//' <<< "$result"
