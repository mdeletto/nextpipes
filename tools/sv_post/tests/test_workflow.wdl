version 1.0

import "../tool.wdl" as SV_Post_Wdl

workflow SV_Post_Workflow_Test {
    input {
        String in_svaba_unfiltered_sv_vcf
        String in_svaba_unfiltered_indel_vcf
        String in_svaba_bam
        String in_manta_sv_indel_vcf
        String ref_target_bed
        String ref_repeat_mask_bed
        String ref_pon_range_tsv
        String ref_filter_list_tsv
        String ref_blacklist_bed_tsv
    }
    call SV_Post_Wdl.SV_Post_Somatic as SV_Post_Somatic_Test {
        input:
            in_svaba_unfiltered_sv_vcf = in_svaba_unfiltered_sv_vcf,
            in_svaba_unfiltered_indel_vcf = in_svaba_unfiltered_indel_vcf,
            in_svaba_bam = in_svaba_bam,
            in_manta_sv_indel_vcf = in_manta_sv_indel_vcf,
            ref_target_bed = ref_target_bed,
            ref_repeat_mask_bed = ref_repeat_mask_bed,
            ref_pon_range_tsv = ref_pon_range_tsv,
            ref_filter_list_tsv = ref_filter_list_tsv,
            ref_blacklist_bed_tsv = ref_blacklist_bed_tsv
    }
    call SV_Post_Wdl.SV_Post_Germline as SV_Post_Germline_Test {
        input:
            in_svaba_unfiltered_sv_vcf = in_svaba_unfiltered_sv_vcf,
            in_svaba_unfiltered_indel_vcf = in_svaba_unfiltered_indel_vcf,
            in_svaba_bam = in_svaba_bam,
            in_manta_sv_indel_vcf = in_manta_sv_indel_vcf,
            ref_target_bed = ref_target_bed,
            ref_repeat_mask_bed = ref_repeat_mask_bed,
            ref_pon_range_tsv = ref_pon_range_tsv,
            ref_filter_list_tsv = ref_filter_list_tsv,
            ref_blacklist_bed_tsv = ref_blacklist_bed_tsv
    }
    output {
        File out_somatic_tsv = SV_Post_Somatic_Test.out_merged_tsv
        File out_germline_tsv = SV_Post_Germline_Test.out_merged_tsv
    }
}