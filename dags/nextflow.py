"""
DAG Definition - Launch Nextflow workflows within Airflow
"""

import json
import os
import subprocess
from logging import Logger
from pathlib import Path

import pendulum
from airflow.decorators import dag, task
from airflow.models.param import Param
from airflow.operators.python import get_current_context

import nextflow
from dotenv import load_dotenv

DEFAULT_ARGS = {
    "owner": "KSG",
    "email": ["mdeletto@ds.dfci.harvard.edu", "gufran@ds.dfci.harvard.edu"],
}

# List nextflow workflows and profiles for Airflow param mapping
NEXTPIPES_DIR = "/opt/airflow/dags/nextpipes"
AVAILABLE_PROFILES = ['google_batch', 'local', 'ksg_gcp_strata']

workflow_directories = [os.path.join(NEXTPIPES_DIR, "workflows")]

workflows = {}

for workflow_directory in workflow_directories:
    for file_name in os.listdir(workflow_directory):
        if file_name.endswith('.nf'):
            workflows[file_name] = os.path.join(workflow_directory, file_name)


@dag(
    dag_id="nextflow",
    description="""A workflow for launching Nextflow pipeline (i.e. Nextpipes) within Airflow""",
    start_date=pendulum.datetime(2022, 1, 1, tz="US/Eastern"),
    # This DAG should be triggered w/ config.
    schedule_interval=None,
    default_args=DEFAULT_ARGS,
    catchup=False,
    max_active_runs=1,
    tags=["nextpipes", "hector"],
    params={
        "workflow_name": Param(type="string", enum=list(workflows.keys())),
        "workflow_inputs": Param(type=["object", "null"]),
        "profiles": Param(["google_batch"], type=["array", "null"], examples=AVAILABLE_PROFILES),
        "help": Param(type="boolean", default=False),
    },
)
def run_nextflow_pipeline():

    @task
    def run_nextflow():
        """
        Runs nextflow
        """
        # Load the .dot env
        load_dotenv(dotenv_path=os.path.join(NEXTPIPES_DIR, ".env.airflow"))
        print(dict(os.environ))

        context = get_current_context()
        workflow_name = context["params"]["workflow_name"]
        workflow_inputs = context["params"]["workflow_inputs"]
        workflow_profiles = context["params"]["profiles"]
        help = context["params"]["help"]

        if help:
            workflow_inputs = {'help': 'help'}

        for execution in nextflow.run_and_poll(workflows[workflow_name], sleep=2,
                                               params=workflow_inputs,
                                               profiles=workflow_profiles,
                                               configs=[os.path.join(os.environ['NEXTPIPES'], 'nextflow.config')]):
            print(execution.log)
            print(execution.stdout)
            print(execution.stderr)

    run_nextflow()

# Call pipeline
run_nextflow = run_nextflow_pipeline()
