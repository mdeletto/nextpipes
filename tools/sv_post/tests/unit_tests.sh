#!/usr/bin/env bash

# shellcheck source=./../../incl.sh
. "$(dirname "$(readlink -f "$0")")"/../../incl.sh

set -o errexit
set -o nounset
set -o pipefail

create_tmp

image_name=$1

run_r_unit_test "$image_name"

run_r_coverage "$image_name"
