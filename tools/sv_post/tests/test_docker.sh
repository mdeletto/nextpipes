#!/usr/bin/env bash

# shellcheck source=./../../incl.sh
. "$(dirname "$(readlink -f "$0")")"/../../incl.sh

set -o errexit
set -o nounset
set -o pipefail

create_tmp

image_name=$1

__in_dir="$THIS_DIR"/test_data/input
__comparison_dir="$THIS_DIR"/test_data/output/

## Somatic
log info "Running somatic test..."

__out_dir="$OUTPUT_FOLDER"/somatic

docker run --rm --network none \
  -v "$__in_dir"/:/in/:ro \
  -v "$__out_dir":/out:rw \
  -v "$REFS_DIR":/refs_folder:ro \
  "$image_name" \
  /app/ensemble_calling.R \
  --svaba-sv /in/TEST.svaba.unfiltered.somatic.sv.vcf \
  --svaba-indel /in/TEST.svaba.unfiltered.somatic.indel.vcf \
  --svaba-bam /in/TEST.contigs.bam \
  --manta-sv-indel /in/TEST.somaticSV.vcf \
  --repeat-mask /refs_folder/hg19_repeat_regions.bed \
  --pon-range /refs_folder/PON_100bpRANGE_5bp_cutoff.txt \
  --breakmer-filter-list /refs_folder/Dec2020_breakmer_filter_list.txt \
  --blacklist /refs_folder/TDG_PON_EXACT_MATCH_5bp_cutoff_breakpoints.bed \
  --target-bed /refs_folder/160620_POPv3_SV_Targets.bed \
  --mode somatic \
  --output /out/output.tsv

diff_test_result "$__out_dir"/output.tsv "$__comparison_dir"/somatic/TEST.ensemble.100bprange.tsv

## Germline
log info "Running germline test..."

__out_dir="$OUTPUT_FOLDER"/germline

docker run --rm --network none \
  -v "$__in_dir"/:/in/:ro \
  -v "$__out_dir":/out:rw \
  -v "$REFS_DIR":/refs_folder:ro \
  "$image_name" \
  /app/ensemble_calling.R \
  --svaba-sv /in/TEST.svaba.unfiltered.somatic.sv.vcf \
  --svaba-indel /in/TEST.svaba.unfiltered.somatic.indel.vcf \
  --svaba-bam /in/TEST.contigs.bam \
  --manta-sv-indel /in/TEST.somaticSV.vcf \
  --repeat-mask /refs_folder/hg19_repeat_regions.bed \
  --pon-range /refs_folder/PON_100bpRANGE_5bp_cutoff.txt \
  --breakmer-filter-list /refs_folder/Dec2020_breakmer_filter_list.txt \
  --blacklist /refs_folder/TDG_PON_EXACT_MATCH_5bp_cutoff_breakpoints.bed \
  --target-bed /refs_folder/160620_POPv3_SV_Targets.bed \
  --mode germline \
  --output /out/output.tsv

diff_test_result "$__out_dir"/output.tsv "$__comparison_dir"/germline/TEST.ensemble.100bprange.tsv

## Empty result test (using also empty input files)
log info "Running empty result test..."

__out_dir="$OUTPUT_FOLDER"/somatic

docker run --rm --network none \
  -v "$__in_dir"/:/in/:ro \
  -v "$__out_dir":/out:rw \
  -v "$REFS_DIR":/refs_folder:ro \
  "$image_name" \
  /app/ensemble_calling.R \
  --svaba-sv /in/TEST.empty.svaba1.vcf \
  --svaba-indel /in/TEST.empty.svaba2.vcf \
  --svaba-bam /in/TEST.contigs.bam \
  --manta-sv-indel /in/TEST.empty.manta.vcf \
  --repeat-mask /refs_folder/hg19_repeat_regions.bed \
  --pon-range /refs_folder/PON_100bpRANGE_5bp_cutoff.txt \
  --breakmer-filter-list /refs_folder/Dec2020_breakmer_filter_list.txt \
  --blacklist /refs_folder/TDG_PON_EXACT_MATCH_5bp_cutoff_breakpoints.bed \
  --target-bed /refs_folder/160620_POPv3_SV_Targets.bed \
  --mode somatic \
  --output /out/output.tsv

diff_test_result "$__out_dir"/output.tsv "$__comparison_dir"/somatic/TEST.empty.output.tsv
