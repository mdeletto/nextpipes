include { read_images } from "$NEXTPIPES/tools/helper"

images = read_images()
output_dir = params.output_dir ? params.output_dir : "."

process run_maftomaf {
  container { images['nextpipes/maftomaf'] ?: task.container }

  input:
  path in_tsv
  val ref_fasta
  val vep_cache_folder

  output:
  path 'output.maf'

  script:
  """
  mkdir /opt/vep/cache
  mkdir /opt/vep/ref_fasta
  gsutil -m cp -r $vep_cache_folder /opt/vep/cache
  gsutil cp $ref_fasta /opt/vep/ref_fasta

  perl /opt/vcf2maf/maf2maf.pl --input-maf $in_tsv --output-maf 'output.maf' --ref-fasta /opt/vep/ref_fasta/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa.gz --vep-path /opt/vep/src/ensembl-vep/ --vep-data /opt/vep/cache/homo_sapiens
  """

}