
## This function takes the following inputs:

# combined.svaba.manta.range.match dataframe output from the annotate_TDG_filter_list.R function
# repeat mask file in the ref dir

## The purpose of this function is to annotate the collapsed and concatenated
## dataframe with the repeat regions reference file by adding two new columns
## REPEAT1 and REPEAT2.  If a mutation falls within a region in the repeat region file
## it gets an entry into that new column, otherwise NA.

## The output of this function is a single dataframe called combined.svaba.manta.range.match

library(dplyr)

annotate_repeats <- function(combined.svaba.manta.range.match, repeat.mask) {

  names(repeat.mask) <- c("chr", "start", "end", "strand", "name", "class", "family")
  repeat.mask$chr <- gsub("chr", "", repeat.mask$chr)

  # loop over range match dataframe and annotated based on repeat mask file
  if (nrow(combined.svaba.manta.range.match) != 0) {

    combined.svaba.manta.range.match$REPEAT1 = NA
    combined.svaba.manta.range.match$REPEAT2 = NA

    for (i in 1:nrow(combined.svaba.manta.range.match)) {

      if (nrow(combined.svaba.manta.range.match) == 0) {
        break
      }

      repeat1 <-
        repeat.mask %>%
          dplyr::filter(chr == as.character(combined.svaba.manta.range.match[i, 'CHROM1'])) %>%
          dplyr::filter(start <= as.numeric(combined.svaba.manta.range.match[i, 'POS1'])) %>%
          dplyr::filter(end >= as.numeric(combined.svaba.manta.range.match[i, 'POS1'])) %>%
        select(class)

      if (nrow(repeat1) >= 1) {
        # take first one
        combined.svaba.manta.range.match[i,]$REPEAT1 = repeat1[1,]$class
      }

      repeat2 <-
        repeat.mask %>%
          dplyr::filter(chr == as.character(combined.svaba.manta.range.match[i, 'CHROM2'])) %>%
          dplyr::filter(start <= as.numeric(combined.svaba.manta.range.match[i, 'POS2'])) %>%
          dplyr::filter(end >= as.numeric(combined.svaba.manta.range.match[i, 'POS2'])) %>%
        select(class)

      if (nrow(repeat2) >= 1) {
        # take first one
        combined.svaba.manta.range.match[i,]$REPEAT2 = repeat2[1,]$class
      }

    }

    return (combined.svaba.manta.range.match)

  }

}
