#!/usr/bin/env bash

# shellcheck source=./../../incl.sh
. "$(dirname "$(readlink -f "$0")")"/../incl.sh

set -o errexit
set -o nounset
set -o pipefail

# First arg will be images.json, assign contents to images_json

# Create empty JSON string
new_manifest="{}"

# Strip keys , produce array of strings
image_name= "$1"
push_output=$(docker push "$image_name" 2>&1)
echo "{\"$image_name\": \"$push_output\"}"