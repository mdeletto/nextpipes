# Executors

***

In the Nextflow framework architecture, the executor is the component that determines the system where a pipeline process is run and supervises its execution.

The executor provides an abstraction between the pipeline processes and the underlying execution system. This allows you to write the pipeline functional logic independently from the actual processing platform.

In other words, you can write your pipeline script once and have it running on your computer, a cluster resource manager, or the cloud — simply change the executor definition in the Nextflow configuration file.

This repository contains several helpful executor profiles to help you get started:

- Local
- LSF
- SLURM
- Google Batch

More information regarding Nextflow's executor support can be found [here](https://www.nextflow.io/docs/latest/executor.html).
