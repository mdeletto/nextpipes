#!/usr/bin/env bash

# shellcheck source=./../../incl.sh
. "$(dirname "$(readlink -f "$0")")"/../../incl.sh

set -o errexit
set -o nounset
set -o pipefail

run_wdl_test

diff_test_result "$OUTPUT_FOLDER"/script_output/out_somatic_SV.tsv "$THIS_DIR"/test_data/output/somatic/TEST.ensemble.100bprange.tsv
diff_test_result "$OUTPUT_FOLDER"/script_output/out_germline_SV.tsv "$THIS_DIR"/test_data/output/germline/TEST.ensemble.100bprange.tsv