#!/usr/bin/env nextflow

def helpMessage() {
    log.info"""
    ===================================
     stitch.nf --options
    ===================================
    Description:

    This workflow is used to perform genotype imputation with the STITCH.R package.

    Usage:

    nextflow run bin/stitch/main.nf -profile "comma,delimited,list,of,profiles"
        -w /path/to/nextflow_workDir
        -c /path/to/nextflow.config
        --output_dir /path/to/output/dir/
        --input_bams '/path/to/*.bam'
        OR
        --bam_list '/path/to/my/file/containing/bam/paths.txt'


    Mandatory arguments:
      -profile                      Suggested profiles are "google_batch_artifact_registry,google_batch,dynamic_memory,std_resources" for GCP or "docker,dynamic_memory,std_resources" for local execution
      --input_bams                  One or more input BAM files (glob patterns are supported)
      OR
      --bam_list                    A text file containing a list of BAM filepaths (one per line).

    When using profile `google_batch` you must also provide a GCS bucket as working directory via:
      -work-dir                     gs://mybucket/nextflow_workDir/

    Other options:
      --output_dir           The output directory where the results will be saved. Default is current working directory.

    """.stripIndent()
}

// Show help message
params.help = false
if (params.help){
    helpMessage()
    exit 0
}

// ARGUMENTS
output_dir = params.output_dir ? params.output_dir : "."

process sliceBamsPerChr_samtools {

  label 'samtools'

  input:
  path bam
  each chr

  output:
  tuple val("${chr}"), path("${bam.simpleName}.${chr}.ba*")

  script:
  """
  samtools index $bam
  samtools view -b $bam $chr > ${bam.simpleName}.${chr}.bam
  samtools index ${bam.simpleName}.${chr}.bam
  """
}

process performGenotypeImputation_stitch {

  label 'stitch'

  input:
  tuple val(chr), path('*'), path(positions_file), path(reference_haplotype_file), path(reference_legend_file), path(reference_sample_file)

  output:
  path 'stitch.*.vcf.gz'

  script:
  """
  find . -name '*.bam' | sort | uniq > bamlist.${chr}.txt

  cat $positions_file | \
    sed 's/:/\\t/g' | sed 's/ /\\t/g' | \
    awk 'BEGIN {FS = "\\t";OFS = "\\t"} length(\$5)==1 && length(\$6)==1 {print \$1,\$2,\$5,\$6}' | \
    sort -k1,1 -k2,2n | \
    awk '!seen[\$2]++' | \
    uniq > ${chr}.posfile

  zcat $reference_legend_file | head -n1 > ${chr}.legend.header.txt
  zgrep Biallelic_SNP $reference_legend_file | \
    grep -E "^${chr}" | \
    awk '!seen[\$2]++' > ${chr}.legend.txt
  cat ${chr}.legend.header.txt ${chr}.legend.txt | gzip > ${chr}.legend.gz

  /STITCH/STITCH.R \
    --K=10 \
    --nGen=1240 \
    --outputdir=./ \
    --nCores=${task.cpus} \
    --chr=${chr} \
    --bamlist=bamlist.${chr}.txt \
    --posfile=${chr}.posfile \
    --reference_sample_file=$reference_sample_file \
    --reference_legend_file=${chr}.legend.gz \
    --reference_haplotype_file=$reference_haplotype_file

  """
}

process mergeVCFs_bcftools {

  label 'bcftools'
  publishDir "$output_dir/", mode : 'symlink', overwrite : true

  input:
  path '*'

  output:
  path 'stitch.vcf.gz*'

  script:
  // STITCH .vcf.gz output doesn't provide index, so create with tabix
  // Then, concat and sort the output VCF
  """
  find . -name 'stitch.*.vcf.gz' -exec bcftools index --tbi --threads ${task.cpus} {} \\;
  bcftools concat --threads ${task.cpus} stitch.*.vcf.gz | bcftools sort -O z -o stitch.vcf.gz
  bcftools index --tbi --threads ${task.cpus} stitch.vcf.gz
  """
}

workflow {

    // Allow for user to supply an --input_bams glob path
    // Or a text file containing input bam paths
    if (params.input_bams && (!params.bam_list) ){
      in_bams = Channel.fromPath(params.input_bams)
    } else if (params.bam_list && (!params.input_bams)){
      in_bams = Channel.fromPath(params.bam_list).splitCsv().map { it[0] }
    } else {
      error "ERROR: Please define input BAMs via --input_bams or provide a text file containing BAM paths with --bam_list"
    }

    // Define ref contigs
    // TODO: Do this dynamically based on reference.fasta
    ref_contigs = Channel.from(1..22)

    // Slice BAMs per chromosome
    sliceBamsPerChr_samtools(in_bams, ref_contigs)

    // Create a chr-specific input channel and run STITCH
    performGenotypeImputation_stitch_in = sliceBamsPerChr_samtools.out.groupTuple().map{
        chr, files -> tuple(
            chr,
            files.flatten(),
            file("gs://gusev-pipeline/1000GP_Phase3/1000GP_Phase3_chr${chr}.HRC_EUR_AF01.pos"),
            file("gs://gusev-pipeline/1000GP_Phase3/1000GP_Phase3_chr${chr}.hap.gz"),
            file("gs://gusev-pipeline/1000GP_Phase3/1000GP_Phase3_chr${chr}.legend.gz"),
            file("gs://gusev-pipeline/1000GP_Phase3/1000GP_Phase3.sample")
        )
    }
    performGenotypeImputation_stitch(performGenotypeImputation_stitch_in)

    // Merge all per chr STITCH .vcf.gz's into a single stitch.vcf.gz
    mergeVCFs_bcftools(performGenotypeImputation_stitch.out.collect())

}
