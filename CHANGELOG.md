# Changelog

This Changelog tracks changes to this project. The notes below include a summary for each release, followed by details which contain one or more of the following tags:

- `added` for new features.
- `changed` for functionality changes.
- `deprecated` for soon-to-be removed features.
- `removed` for now removed features.
- `fixed` for any bug fixes.

## 15 Feb 2023

- `removed` resources configs, asserting each cluster will contain its own proper definitions
- `removed` publishDir directives from all but final process
- `added` resource definitions and queue "compute" to parallelcluster config
- `changed` cluster script
  - use c5.4xlarge (not c5.9xlarge) in queue for "compute" nodes
  - add m5.2xlarge (not c5.9xlarge) in queue for "regular" nodes
  - use r5.xlarge (not c5.9xlarge) for head node
  - use 256GB (not 1TB) disk on the head node  
- `added` gatk installation to on-node-configured
- `changed` sv_caller script for removed resources configs

### For next test

- `changed` cluster script for next test
  - use 1TB (not 15TB) shared on the nodes -- only 16 (not 368) samples
  - limited max node count to 8 for even plexing
  - change region and subnet to us-east-2 (default subnet)
  - change ssh keyname to new us-east-2 key
- `changed` realign_bam script for next test
