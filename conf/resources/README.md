# Resources
***

Here you can define profiles associated with resource allocation for processes. Resource allocations can be defined
statically or dynamically, and can be associated with labels or process names directly.

This repository contains the following resource profiles to help you get started:

## The `std_resource` profile

In the following example, processes with the `bash` label will have a CPU number set to 1.
This is a static resource definition.

      std_resources {
        process {
          withLabel : bash {
            cpus = 1
          }
        }
      }

## The `dynamic_memory` profile

The following profile is an example of a dynamic resource allocation where the memory for a given process
is initially allocated to 8 GB. The process will be retried if a failure is encountered up to a max of 3 times
(or 24 GB of memory by the third attempt). If the process does not succeed after the third try, it will fail.

      dynamic_memory {
        process {
          // Default memory
          java_memory = "8GB".replaceAll("B", "").replaceAll(" ", "")
          memory = {
            8. GB * task.attempt
          }
          // Memory is dynamic based on retry attempts
          maxRetries = 3
          errorStrategy = "retry"
        }
      }