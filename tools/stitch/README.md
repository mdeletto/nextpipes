# stitch.nf
***

## Overview

This workflow is used to run the STITCH.R package for haplotype imputation. The workflow runs the following processes:

1) `sliceBamsPerChr_samtools` - Splits input BAM by chromosome and indexes the output BAM. 
The input channel to the next process contains all sliced BAMs per chromosome.
2) `performGenotypeImputation_stitch` - Performs cleanup of STITCH reference files and then runs the STITCH.R algorithm.
An output `stitch.${chr}.vcf.gz` is produced.
3) `mergeVCFs_bcftools` - Indexes the per-chromosome `stitch.${chr}.vcf.gz` files, concatenates them, and sorts the output
to produce a combined `stitch.vcf.gz` and its respective tabix index file `.tbi`.

***

## Examples of how to run

Please consult the `main.nf`'s help documentation for detailed usage (`nextflow run /path/to/module/main.nf --help`).

### Local usage

    nextflow run bin/stitch/main.nf \ 
        -profile docker,dynamic_memory,std_resources \
        -w 'gs://nextpipes/workDir' \
        -c nextflow.config \
        --output_dir gs://nextpipes/outputDirTest
        --input_bams 'gs://nextpipes/workDir/stage-5e6a802f-a168-429a-ae1c-c79439f2a60c/**/*.bam' \
        OR
        --bam_list '/path/to/file/containing/bam/list'

### Google Batch usage

    nextflow run bin/stitch/main.nf \ 
        -profile google_batch_artifact_registry,google_batch,dynamic_memory,std_resources \
        -w 'gs://nextpipes/workDir' \
        -c nextflow.config \
        --output_dir gs://nextpipes/outputDirTest
        --input_bams 'gs://nextpipes/workDir/stage-5e6a802f-a168-429a-ae1c-c79439f2a60c/**/*.bam' \
        OR
        --bam_list '/path/to/file/containing/bam/list'

### Some helpful tips

To generate a BAM list for input with `--bam_list` from a PROFILE transfer request, you can supply the `transferred_samples.csv` 
you receive from the Transfer team to the following command:

    awk -F, 'FNR > 1 {printf "gs://%s/%s/aligned/%s*.bam\n", $5, $2, $2}' transferred_samples.csv | \
    xargs -I {} gsutil ls -p profile-cloud-pilot {}` > bamlist.txt

The above command will search the corresponding bucket for matching samples based on the expected directory structure for PROFILE BAMs.