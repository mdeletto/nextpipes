import groovy.json.JsonSlurper

params.images_json = "$NEXTPIPES/images.json"

def read_images() {
    return new JsonSlurper().parse(new File(params.images_json))
}